package com.rest.meamouz.constant;

public class ClassTime {
    public static final int TIME_1 = 0;
    public static final int TIME_2 = 1;
    public static final int TIME_3 = 2;
    public static final int TIME_4 = 3;
    public static final int TIME_5 = 4;
    public static final int TIME_6 = 5;
    public static final int TIME_7 = 6;
}
