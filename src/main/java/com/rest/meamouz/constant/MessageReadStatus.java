package com.rest.meamouz.constant;

public class MessageReadStatus {

    public static final int UNREAD = 0;
    public static final int READ = 1;
}
