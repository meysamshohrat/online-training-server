package com.rest.meamouz.constant;

public class PaymentType {

    public static final int ONLINE = 0;

    public static final int CASH = 1;
}
