package com.rest.meamouz.constant;

public class TeachMeetingStatus {
    public static final int WAITING_TO_ACCEPT = 0;
    public static final int ACCEPTED = 1;
    public static final int DENIED = 2;
    public static final int TEACHER_ON_THE_WAY = 3;
    public static final int STARTED = 4;
    public static final int COMPLETED = 5;
}
