package com.rest.meamouz.controller;

import com.google.gson.Gson;
import com.rest.meamouz.exception.StudentNotFoundException;
import com.rest.meamouz.exception.StudentUsernameExistsException;
import com.rest.meamouz.model.ApiError;
import com.rest.meamouz.model.Student;
import com.rest.meamouz.model.request.LoginRequest;
import com.rest.meamouz.model.request.SignupRequest;
import com.rest.meamouz.model.response.GeneralResponse;
import com.rest.meamouz.model.response.SimpleResponse;
import com.rest.meamouz.model.response.TokenResponse;
import com.rest.meamouz.repository.StudentRepository;
import com.rest.meamouz.repository.StudentUserRepository;
import com.rest.meamouz.repository.TeachMeetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.List;

import static com.rest.meamouz.constant.SecurityConstants.HEADER_STRING;
import static com.rest.meamouz.constant.SecurityConstants.TOKEN_PREFIX;

@Controller
@RequestMapping(path = "/student")
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeachMeetingRepository teachMeetingRepository;

    @Autowired
    private StudentUserRepository studentUserRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping(path = "/sign-up",produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> signupStudent(@NotNull(message = "request body must not be null") @Valid @RequestBody SignupRequest signupRequest)
    {
        Student oldUser = studentRepository.findByUsername(signupRequest.getUsername());
        if(oldUser != null)
        {
            throw new StudentUsernameExistsException();
        }
        Student student = new Student();
        student.setUsername(signupRequest.getUsername());
        student.setFirstName(signupRequest.getFirstName());
        student.setLastName(signupRequest.getLastName());
        student.setPhoneNumber(signupRequest.getPhone());
        student.setPassword(bCryptPasswordEncoder.encode(signupRequest.getPassword()));
        studentRepository.save(student);
        return ResponseEntity.ok(new GeneralResponse.Builder().status(HttpStatus.OK.value()).data(new SimpleResponse("حساب کاربری با موفقیت ساخته شد")).build().toJson());
    }

    @PostMapping(path = "/login",produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> loginStudent(@NotNull(message = "request body must not be null") @Valid @RequestBody LoginRequest loginRequest)
    {
        try {
            ResponseEntity<Object> loginResponse = restTemplate.postForEntity("http://localhost:8080/login", loginRequest, Object.class);
            if (loginResponse.getStatusCode() == HttpStatus.OK && loginResponse.getHeaders().get(HEADER_STRING) != null) {
                List<String> headers = loginResponse.getHeaders().get(HEADER_STRING);
                String token = null;
                for (String header : headers) {
                    if (header.startsWith(TOKEN_PREFIX)) {
                        token = header.replace(TOKEN_PREFIX, "");
                        break;
                    }
                }
                TokenResponse tokenResponse = new TokenResponse();
                Student student = studentRepository.findByUsername(loginRequest.getUsername());
                if(student != null)
                {
                    tokenResponse.setId(student.getId());
                    tokenResponse.setUsername(student.getUsername());
                    tokenResponse.setFistName(student.getFirstName());
                    tokenResponse.setToken(token);
                }
                return ResponseEntity.ok(new GeneralResponse.Builder().status(HttpStatus.OK.value()).data(tokenResponse).build().toJson());
            }
            else
            {
                throw new StudentNotFoundException();
            }
        } catch (RestClientException ex)
        {
            throw new StudentNotFoundException();
        }
    }

    @GetMapping(path = "/test",produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> test()
    {
        return ResponseEntity.ok(new GeneralResponse.Builder().status(HttpStatus.OK.value()).data(new SimpleResponse("It works fine")).build().toJson());
    }

}
