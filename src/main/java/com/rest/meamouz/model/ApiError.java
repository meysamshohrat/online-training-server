package com.rest.meamouz.model;

public class ApiError {

    private int status;
    private String error;

    public ApiError(int status, String error) {
        super();
        this.status = status;
        this.error = error;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "status=" + status +
                ", error='" + error + '\'' +
                '}';
    }
}
