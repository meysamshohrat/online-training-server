package com.rest.meamouz.model;

import com.rest.meamouz.constant.Gender;

import javax.persistence.*;
import java.util.List;

@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String username;

    private String firstName;

    private String lastName;

    private String password;

    private String email;

    private String phoneNumber;

    private int gender = Gender.UNKNOWN;

    private String aboutMe;

    private long credit;

    @OneToMany(mappedBy="studentId", cascade = CascadeType.ALL)
    private List<Message> messages;

    @OneToMany(mappedBy="studentId", cascade = CascadeType.ALL)
    private List<TeachMeeting> activeMeetings;

    @OneToMany(mappedBy="studentId", cascade = CascadeType.ALL)
    private List<TeachMeeting> historyMeetings;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public long getCredit() {
        return credit;
    }

    public void setCredit(long credit) {
        this.credit = credit;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<TeachMeeting> getActiveMeetings() {
        return activeMeetings;
    }

    public void setActiveMeetings(List<TeachMeeting> activeMeetings) {
        this.activeMeetings = activeMeetings;
    }

    public List<TeachMeeting> getHistoryMeetings() {
        return historyMeetings;
    }

    public void setHistoryMeetings(List<TeachMeeting> historyMeetings) {
        this.historyMeetings = historyMeetings;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
