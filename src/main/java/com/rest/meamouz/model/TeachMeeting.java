package com.rest.meamouz.model;

import com.rest.meamouz.constant.ClassTime;
import com.rest.meamouz.constant.TeachMeetingStatus;

import javax.persistence.*;

@Entity
public class TeachMeeting {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private int status = TeachMeetingStatus.WAITING_TO_ACCEPT;

    private int teacherId;

    @JoinColumn
    private int studentId;

    private int timeOfClass = ClassTime.TIME_1;

    private String dateOfClass;

    private int price;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getTimeOfClass() {
        return timeOfClass;
    }

    public void setTimeOfClass(int timeOfClass) {
        this.timeOfClass = timeOfClass;
    }

    public String getDateOfClass() {
        return dateOfClass;
    }

    public void setDateOfClass(String dateOfClass) {
        this.dateOfClass = dateOfClass;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }
}
