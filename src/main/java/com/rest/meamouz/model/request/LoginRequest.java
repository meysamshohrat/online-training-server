package com.rest.meamouz.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginRequest {

    @NotNull(message = "نام کاربری نمی‌تواند خالی باشد")
    @Size( min = 2,max = 25,message = "نام کاربری می‌بایست بیش از ۲ و کمتر از ۲۵ کاراکتر باشد")
    private String username;

    @NotNull(message = "رمز عبور نمی‌تواند خالی باشد")
    @Size( min = 8, max = 25, message = "رمز عبور می‌بایست بیش از ۲ و کمتر از ۲۵ کاراکتر باشد")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
