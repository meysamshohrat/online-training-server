package com.rest.meamouz.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SignupRequest {

    @NotNull(message = "نام کاربری نمی‌تواند خالی باشد")
    @Size( min = 2,max = 25,message = "نام کاربری می‌بایست بیش از ۲ و کمتر از ۲۵ کاراکتر باشد")
    private String username;

    @NotNull(message = "شماره همراه نمی‌تواند خالی باشد")
    @Size( min = 11, max = 11, message = "شماره همراه معتبر نیست")
    private String phone;

    @NotNull(message = "رمز عبور نمی‌تواند خالی باشد")
    @Size( min = 8, max = 25, message = "رمز عبور می‌بایست بیش از ۲ و کمتر از ۲۵ کاراکتر باشد")
    private String password;

    @NotNull(message = "نام نمی‌تواند خالی باشد")
    @Size( min = 2, max = 25,message = "نام می‌بایست بیش از ۲ و کمتر از ۲۵ کاراکتر باشد")
    private String firstName;

    @NotNull(message = "نام خانوادگی نمی‌تواند خالی باشد")
    @Size( min = 2, message = "نام خانوادگی می‌بایست بیش از ۲ و کمتر از ۲۵ کاراکتر باشد")
    private String lastName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
