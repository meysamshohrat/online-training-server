package com.rest.meamouz.model.response;

import com.google.gson.Gson;
import org.springframework.http.HttpStatus;

public class GeneralResponse {

    private int status = HttpStatus.OK.value();

    private Object data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String toJson()
    {
        return new Gson().toJson(this);
    }

    public static class Builder {

        private GeneralResponse response;

        public Builder()
        {
            response = new GeneralResponse();
        }

        public Builder status(int status)
        {
            response.setStatus(status);
            return this;
        }

        public Builder data(Object data)
        {
            response.setData(data);
            return this;
        }

        public GeneralResponse build()
        {
            return response;
        }

    }
}
