package com.rest.meamouz.model.response;

public class TokenResponse {

    private Integer id;

    private String username;

    private String fistName;

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    @Override
    public String toString() {
        return "TokenResponse{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", fistName='" + fistName + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
