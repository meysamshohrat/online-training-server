package com.rest.meamouz.repository;

import com.rest.meamouz.model.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student,Integer> {

    Student findByUsername(String username);
}
