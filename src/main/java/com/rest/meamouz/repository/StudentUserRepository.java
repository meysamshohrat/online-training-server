package com.rest.meamouz.repository;

import com.rest.meamouz.model.StudentUser;
import org.springframework.data.repository.CrudRepository;

public interface StudentUserRepository extends CrudRepository<StudentUser,Integer> {

    StudentUser findByUsername(String username);
}
