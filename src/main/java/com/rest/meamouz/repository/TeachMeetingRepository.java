package com.rest.meamouz.repository;

import com.rest.meamouz.model.TeachMeeting;
import org.springframework.data.repository.CrudRepository;

public interface TeachMeetingRepository extends CrudRepository<TeachMeeting,Integer> {
}
