package com.rest.meamouz.service;

import com.rest.meamouz.exception.StudentNotFoundException;
import com.rest.meamouz.model.Student;
import com.rest.meamouz.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Student user = studentRepository.findByUsername(username);
        if(user == null)
        {
            throw new StudentNotFoundException();
        }
        return new User(user.getUsername(),user.getPassword(), Collections.emptyList());
    }
}
